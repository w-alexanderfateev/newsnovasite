<html>
<head>
    <?php
    get_header();
    ?>
</head>
<body>
<main class="page-main">
    <div class="content">
        <div class="main__topline">

            <div class="topline__block">
                <h2 class="topline__header">БЛОГ</h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 21;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsBLOG = get_posts($args);

                    $i = 0;
                    ?>
                    <?php
                    foreach ($postsBLOG as $post) {
                        setup_postdata($post);
                        if ($post->ID <> $real_id) {
                            if ($i < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                            }
                            $i++;
                        }
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>


            <div class="topline__block">
                <h2 class="topline__header">МІНІСТЕРСТВО ВЕТЕРАНІВ</h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 23;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsMINVET = get_posts($args);

                    $i2 = 0;
                    ?>
                    <?php
                    foreach ($postsMINVET as $post) {
                        setup_postdata($post);
                        if ($post->ID <> $real_id) {
                            if ($i2 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i2++;
                            }
                        }
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>

            <div class="topline__block">
                <h2 class="topline__header">ФРОНТ</h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 24;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsFRONT = get_posts($args);

                    $i3 = 0;
                    ?>
                    <?php
                    foreach ($postsFRONT as $post) {
                        setup_postdata($post);
                        if ($post->ID <> $real_id) {
                            if ($i3 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i3++;
                            }
                        }
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>
        </div>
        <div class="main-wrapper">
            <section class="main__column">
                <h1 class="info404">404 Page Not Found</h1>
            </section>
            <section class="side__column">
                <div class="side__block side__block--video">
                    <h2 class="topline__header">Відео</h2>
                    <?php
                    if (function_exists('dynamic_sidebar'))
                        dynamic_sidebar('space-for-video');
                    ?>
                    <a href="#" class="btn btn--all-video">Більше відео</a>
                </div>

                <div class="side__block side__block--news-feed">
                    <h2 class="topline__header">Важливе</h2>

                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 22;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $posts = get_posts($args);

                    $i = 0;

                    foreach ($posts as $post) {
                        setup_postdata($post);
                        if ($post->ID <> $real_id) {
                            if ($i < 3) {
                                ?>
                                <div class="news__post">
                                    <div class="post-wrapper">
                                        <?php
                                        the_post_thumbnail();
                                        ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="post__title">
                                        <h1>
                                            <?php the_title(); ?>
                                        </h1>
                                    </a>
                                </div>
                                <?php
                                $i++;
                            }
                        }
                    }
                    wp_reset_postdata();
                    ?>

                </div>
            </section>
        </div>
    </div>

</main>
<?php
get_footer();
?>

</body>
</html>