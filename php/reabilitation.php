<?php
/*
Template Name: reabilitation
*/
?>
<!DOCTYPE html>
<html>
<head>
    <?php get_header(); ?>
</head>
<body>

<main class="page-main">
    <div class="content">
        
        <div class="main__topline">

            <div class="topline__block">
                <?php
					$category_id = get_cat_ID('Блог');
					$category_link = get_category_link($category_id);
				?>
				<h2 class="topline__header"><a href="<?php echo $category_link; ?>">БЛОГ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 4;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsBLOG = get_posts($args);

                    $i = 0;
                    ?>
                    <?php
                    foreach ($postsBLOG as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                            if ($i < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                            }
                            $i++;
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>


            <div class="topline__block">
					
				<?php
					$category_id = get_cat_ID('Міністерство ветеранів');
					$category_link = get_category_link($category_id);
				?>	
			
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">МІНІСТЕРСТВО ВЕТЕРАНІВ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 6;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsMINVET = get_posts($args);

                    $i2 = 0;
                    ?>
                    <?php
                    foreach ($postsMINVET as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                            if ($i2 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i2++;
                            }
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>

            <div class="topline__block">
				<?php
					$category_id = get_cat_ID('Фронт');
					$category_link = get_category_link($category_id); 
				?>
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">ФРОНТ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 5;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsFRONT = get_posts($args);

                    $i3 = 0;
                    ?>
                    <?php
                    foreach ($postsFRONT as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                            if ($i3 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i3++;
                            }
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
				?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>
        </div>
		
        <div class="main-wrapper">
            <section class="main__column">
                <div class="main-select-wrapper">
                    <h2 class="topline__header">
                        <?php
                        global $wp_query;
                        if ($wp_query->found_posts > 0) {
                            echo 'Останні Новини';
                        } else
                            echo 'Нічого не знайдено...';
                        ?>
                    </h2>
                    <select class="archieve-select" name="selection" id="adssad" onchange="location = this.value;">
                        <option value="region">Оберіть область</option>
                        <?php

                        $tags = get_tags();
                        foreach ($tags as $tag) {
                            if (strpos($tag->name, " область")) {
                                $tag_link = get_tag_link($tag->term_id);
                                ?>
                                <option value="<?php echo $tag_link; ?>">
                                    <?php echo $tag->name ?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="news-feed">
                    <?php $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 140;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsFRONT = get_posts($args);

                    foreach ($postsFRONT as $post) {
                        setup_postdata($post);
                        ?>
                        <div class="news__post">
                            <div class="post-wrapper">
                                <?php
                                the_post_thumbnail();
                                ?>
                                <div class="post__content">
                                    <h2 class="post__title">
                                        <a href="<?php the_permalink(); ?>" style="color: black">
                                            <?php the_title(); ?>
                                        </a>
                                    </h2>
                                    <div class="post__info">
                                        <p class="post__date"><?php the_time('F jS, Y'); ?></p>
                                        <div class="tag-list">
                                            <?php

                                            if (get_the_tag_list()) {
                                                echo get_the_tag_list('<ul class="tag_list"><li><span>', '</span></li><li><span>', '</span></li></ul>');
                                            }

                                            ?>

                                        </div>
                                    </div>
                                    <div class="post__controls">
                                        <a href="<?php the_permalink(); ?>"
                                           class="btn share-list">Детальніше</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="pagination-block">
                    <?php the_posts_pagination(); ?>
                </div>
            </section>
            <?php get_sidebar(); ?>
        </div>
    </div>

</main>
<?php
get_footer();
?>
</body>
</html>