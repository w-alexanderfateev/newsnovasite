<?php

add_action('wp_enqueue_scripts', 'theme_name_styles');
add_action('wp_enqueue_scripts', 'theme_name_scripts');
add_action('after_setup_theme', 'test_after_setup');

add_action('widgets_init', 'register_my_widget_for_search');
add_action('widgets_init', 'register_my_widget_for_video');
add_action('widgets_init', 'register_my_widget_for_regions');
add_action('widgets_init', 'register_my_widget_for_important');
add_action('widgets_init', 'register_my_widget_for_image');
add_action('widgets_init', 'register_my_widget_for_front');
add_action('widgets_init', 'register_my_widget_for_min_vet');
add_action('widgets_init', 'register_my_widget_for_blog');
add_action('widgets_init', 'register_my_widget_for_archive');


//add_filter('show_admin_bar', '__return_true');
if(!current_user_can('administrator')){
    show_admin_bar(false);
}

add_theme_support('widgets');
add_theme_support( 'post-thumbnails' );

add_filter( 'excerpt_length', function(){
	return 20;
});

add_filter('excerpt_more', function($more) {
	return '...';
});

add_filter( 'excerpt_more', 'new_excerpt_more' );

function new_excerpt_more( $more ){
	global $post;
	return '... <a class="" href="'. get_permalink($post) . '">Детальнiше</a>';
}

function test_media()
{
    wp_enqueue_style('test-main', get_stylesheet_uri());

    wp_enqueue_script('test-script-main', get_template_directory_uri() . '/assets/js/main.js');
}

function test_after_setup()
{
    register_nav_menu('top', 'for head');
}

function theme_name_styles()
{
    //wp_enqueue_style('owl.carousel.min-style', get_template_directory_uri() . '/assets/css/carousel/owl.carousel.min.css');
    wp_enqueue_style('owl.theme.default.min-style', get_template_directory_uri() . '/assets/css/carousel/owl.theme.default.min.css');
    wp_enqueue_style('libs-style', get_template_directory_uri() . '/assets/css/libs.css');
    wp_enqueue_style('all-ukraine-style', get_template_directory_uri() . '/assets/css/all-ukraine.css');
    //wp_enqueue_style('defaults-style', get_template_directory_uri() . '/assets/css/defaults.css');
    wp_enqueue_style('video-style', get_template_directory_uri() . '/assets/css/video.css');
    wp_enqueue_style('header-style', get_template_directory_uri() . '/assets/css/header.css');
    wp_enqueue_style('footer-style', get_template_directory_uri() . '/assets/css/footer.css');
    wp_enqueue_style('page-main-style', get_template_directory_uri() . '/assets/css/page-main.css');
    wp_enqueue_style('single-style', get_template_directory_uri() . '/assets/css/single.css');

    wp_enqueue_style('main-style', get_stylesheet_uri());
}


function theme_name_scripts()
{

    wp_register_script('owl-carousel-min', get_template_directory_uri() . '/assets/js/carousel/owl.carousel.min.js');
    wp_enqueue_script('owl-carousel-min');

    wp_register_script('common-script', get_template_directory_uri() . '/assets/js/common.js');
    wp_enqueue_script('common-script');

    wp_register_script('libs-script', get_template_directory_uri() . '/assets/js/libs.js');
    wp_enqueue_script('libs-script');

}

function register_my_widget_for_search()
{
    register_sidebar(
        array(
            'id' => 'space-for-search',
            'name' => "Область для поиска"
        )
    );
}

function register_my_widget_for_video()
{
    register_sidebar(
        array(
            'id' => 'space-for-video',
            'name' => "Область для видео"
        )
    );
}

function register_my_widget_for_important()
{
    register_sidebar(
        array(
            'id' => 'space-for-important',
            'name' => "Область для важного"
        )
    );
}

function register_my_widget_for_image()
{
    register_sidebar(
        array(
            'id' => 'space-for-image',
            'name' => "Область для фото"
        )
    );
}

function register_my_widget_for_blog()
{
    register_sidebar(
        array(
            'id' => 'space-for-blog',
            'name' => "Область для важных видео"
        )
    );
}

function register_my_widget_for_min_vet()
{
    register_sidebar(
        array(
            'id' => 'space-for-min-vet',
            'name' => "Область для min vet"
        )
    );
}

function register_my_widget_for_front()
{
    register_sidebar(
        array(
            'id' => 'space-for-front',
            'name' => "Область для front"
        )
    );
}

function register_my_widget_for_archive()
{
    register_sidebar(
        array(
            'id' => 'space-for-archive',
            'name' => "Область для archive"
        )
    );
}

function register_my_widget_for_regions()
{
    register_sidebar(
        array(
            'id' => 'space-for-regions',
            'name' => "Область для regions"
        )
    );
}
