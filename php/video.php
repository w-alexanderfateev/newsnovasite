<?php
/*
Template Name: video-page
*/
?>
<!DOCTYPE html>
<html>
<head>
    <?php get_header(); ?>
</head>
<body>

<main class="page-video">
    <div class="content">

        <div class="content-wrapper">
            <div class="recommended-videos">
                <h2 class="page__title">Важливе</h2>
                <ul class="video-list">
                    <?php echo do_shortcode('[sp_html5video category="3" limit="3"]'); ?>
                </ul>
            </div>
            <div class="all-videos">
                <h2 class="page__title">Всі Відео</h2>
                <ul class="video-list">
                    <?php echo do_shortcode('[sp_html5video]'); ?>
                </ul>
            </div>
        </div>
    </div>


</main>

<?php
get_footer();
?>
</body>
</html>