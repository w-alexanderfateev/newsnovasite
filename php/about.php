<?php
/*
Template Name: about
*/
?>
<!DOCTYPE html>
<html>
<head>
    <?php get_header(); ?>
</head>
<body>

<main class="page-main">
    <div class="content">
        <div class="main__topline">

            <div class="topline__block">
                <?php
                $category_id = get_cat_ID('Точка зору');
                $category_link = get_category_link($category_id);
                ?>
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">ТОЧКА ЗОРУ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 4;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsBLOG = get_posts($args);

                    $i = 0;
                    ?>
                    <?php
                    foreach ($postsBLOG as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                        if ($i < 3) {
                            ?>
                            <div class="topline__post-wrapper">
                                <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                <div class="topline__post">
                                    <?php the_post_thumbnail(); ?>
                                    <h3 class="post__heading">
                                        <a href="<?php the_permalink(); ?>" class="post__title">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>


            <div class="topline__block">

                <?php
                $category_id = get_cat_ID('Міністерство ветеранів');
                $category_link = get_category_link($category_id);
                ?>

                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">МІНІСТЕРСТВО ВЕТЕРАНІВ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 6;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsMINVET = get_posts($args);

                    $i2 = 0;
                    ?>
                    <?php
                    foreach ($postsMINVET as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                        if ($i2 < 3) {
                            ?>
                            <div class="topline__post-wrapper">
                                <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                <div class="topline__post">
                                    <?php the_post_thumbnail(); ?>
                                    <h3 class="post__heading">
                                        <a href="<?php the_permalink(); ?>" class="post__title">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <?php
                            $i2++;
                        }
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>

            <div class="topline__block">
                <?php
                $category_id = get_cat_ID('Фронт');
                $category_link = get_category_link($category_id);
                ?>
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">ФРОНТ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 5;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsFRONT = get_posts($args);

                    $i3 = 0;
                    ?>
                    <?php
                    foreach ($postsFRONT as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                        if ($i3 < 3) {
                            ?>
                            <div class="topline__post-wrapper">
                                <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                <div class="topline__post">
                                    <?php the_post_thumbnail(); ?>
                                    <h3 class="post__heading">
                                        <a href="<?php the_permalink(); ?>" class="post__title">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <?php
                            $i3++;
                        }
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>

        </div>

        <div class="main-wrapper">
            <section class="main__column">
                <div class="main-select-wrapper">
                    <h2 class="topline__header">Про нас</h2>
                </div>
                <div class="news-feed" style="padding: 15px;text-align: justify;">
                       <p style="text-indent: 15px">Як відомо, і ті, хто повертаються з фронту - все одно залишаються там, на війні. 
                    Так сталося, що команда добровольців, які пішли захищати Україну у 14 році, переконалися, 
                    що їхні погляди та світосприймання змінилося назавжди.</p>
                        <p style="text-indent: 15px">Тепер ми знаємо, що справжня свобода й незалежність країни коштує не тільки великих витрат, 
                    втрачених людських життів, а й обміркованої, виваженої державної стратегії. 
                    Першочерговим завданням держави є переконання своїх громадян щодо необхідності захисту своєї свободи, власності та незалежності.
                    Хтось можливо скаже, так все це повинна забезпечувати держава -  з її органами влади, силовими відомствами та урядом. 
                    В теорії - так воно і є. Але в дійсності, навіть у США, однієї з найрозвиненіших та найдемократичніших країн світу, 
                    гарантією громадянських прав та свобод  історично виступало та виступає практично вільне володіння громадянами зброєю.</p>
                        <p style="text-indent: 15px">Усвідомлення того, що свою країну та свою власність ми маємо привчитися боронити самостійно, власними діями - 
                    і є ознака нашої цивілізованості. Тим більше зараз, у цей період, коли на Сході продовжується протистояння з озброєними 
                    на навченими Росією сепаратистами.</p> 
                        <p style="text-indent: 15px">І ті, хто пройшов війну, і ті, хто служив і продовжує служити з 14 року у 
                    ЗСУ, ДПС, МВС, інших силових відомствах - вже віддали свій борг: забезпечили існування сучасної України. 
                    Ми насправді хочемо, щоб кожен громадянин та громадянка нашої держави також зробили такий невеличкий внесок. 
                    Все це можливо: так існують зараз і Швейцарія, і Ізраїль, так повинні робити й ми - якщо над нами майорить жовто-блакитний прапор.</p>
                </div>
                </section>
            <?php get_sidebar(); ?>
        </div>
    </div>

</main>

<?php
get_footer();
?>
</body>
</html>