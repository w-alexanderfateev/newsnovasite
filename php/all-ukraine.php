<?php
/*
Template Name: all-ukraine
*/
?>
<!DOCTYPE html>
<html>
<head>
    <?php get_header(); ?>
</head>
<body>
<main class="page-all-ukraine">
    <div class="content">

        <div class="news-feed">
            <select class="archieve-select" name="selection" id="adssad" onchange="location = this.value;">
                <option value="region">Оберіть область</option>
                <?php

                $tags = get_tags();
                foreach ($tags as $tag) {
                    if (strpos($tag->name, " область")) {
                        $tag_link = get_tag_link($tag->term_id);
                        ?>
                        <option value="<?php echo $tag_link; ?>">
                            <?php echo $tag->name ?>
                        </option>
                        <?php
                    }
                }
                ?>
            </select>

            <?php

            query_posts('cat=35');
            while (have_posts()) {

                the_post();
                if (get_the_title() != "Вся Україна") {
                    ?>
                    <div class="news__post">
                        <div class="post-wrapper">
                            <?php
                            the_post_thumbnail();
                            ?>
                            <div class="post__content">
                                <h2 class="post__title">
                                    <a href="<?php the_permalink(); ?>" style="color: black">
                                        <?php the_title(); ?>
                                    </a>
                                </h2>
                                <div class="post__info">
                                    <p class="post__date"><?php the_time('F jS, Y'); ?></p>
                                    <div class="tag-list">
                                        <?php

                                        if (get_the_tag_list()) {
                                            echo get_the_tag_list('<ul class="tag_list"><li><span>', '</span></li><li><span>', '</span></li></ul>');
                                        }

                                        ?>

                                    </div>
                                </div>
                                <div class="post__controls">
                                    <a href="<?php the_permalink(); ?>"
                                       class="btn post__read-more">Детальніше</a>
                                    <div class="share-list">
                                        <a href="#" class="share__link share__link--facebook">
                                            <span class="link__text">share</span>
                                        </a>
                                        <a href="#" class="share__link share__link--youtube">
                                            <span class="link__text">share</span>
                                        </a>
                                        <a href="#" class="share__link share__link--googleplus">
                                            <span class="link__text">share</span>
                                        </a>
                                        <a href="#" class="share__link share__link--telegram">
                                            <span class="link__text">share</span>
                                        </a>
                                        <a href="#" class="share__link share__link--instagram">
                                            <span class="link__text">share</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } ?>

        </div>
        <div class="pagination">
            <?php the_posts_pagination(); ?>
        </div>
    </div>
</main>

<?php
get_footer();
?>
</body>
</html>
