<?php
wp_footer();
?>
<footer class="authority">
    <div class="content">
        <div class="contact-block">
            <div class="text-block">
				<h2 class="topline__header"><a href="/pro-nas/">Про нас</a></h2>
                <h2 class="topline__header"><a href="/kontakty/">Контакти</a></h2>
                <h2 class="topline__header"><a href="/partnery/">Партнери</a></h2>
            </div>
            <!--
			<div class="map-block">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1524.6927216658312!2d36.227566979589525!3d50.007282421325826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a12002b71ac5%3A0xa76340b792d8ace8!2z0JTQtdGA0LbQv9GA0L7QvA!5e0!3m2!1sru!2sua!4v1557181664666!5m2!1sru!2sua"
                        frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
            -->
        </div>
        <div class="partners-block">
            <ul class="partners__list">
                <!--<li class="partners__item partners__item-imgs">
                    
					<a href="http://dopomoga-ato.com.ua/" class="partners__link">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/content/1.jpg" alt="#" class="partners__img">
                    </a>
                    <a href="http://dsvv.gov.ua/" class="partners__link">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/content/2.jpg" alt="#" class="partners__img">
                    </a>
					<a href="http://komvti.rada.gov.ua/" class="partners__link">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/content/3.jpg" alt="#" class="partners__img">
                    </a>
                </li>-->
				<li class="partners__item partners__item-min">
                    <a href="http://www.mil.gov.ua" class="partners__link partners__link-min btn">
                        MIНIСТЕРСТВО ОБОРОНИ України
                    </a>
                </li>
                <li class="partners__item partners__item-min">
                    <a href="https://mva.gov.ua/" class="partners__link partners__link-min btn">
                        MIНIСТЕРСТВО У СПРАВАХ ВЕТЕРАНIВ
                    </a>
                </li>
				<li class="partners__item partners__item-min">
                    <a href="http://svato.kh.ua/language/uk/" class="partners__link partners__link-min btn">
                        Харкiвська обласна спiлка ветеранiв АТО
                    </a>
                </li>
            </ul>
        </div>
        <div class="authority-wrapper">
            <p class="authority__text">
                Powered by <a href="http://nure.ua/faculty/fakultet-kompyuternoyi-inzheneriyi-ta-upravlinnya"
                              class="authority__link">Nure</a> &copy; 2019
            </p>
        </div>
		
    </div>
</footer>