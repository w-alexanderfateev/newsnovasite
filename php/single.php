<html>
<head>
    <?php
    get_header();
    ?>
</head>
<body class="single-page">
<main class="page-main">
    <div class="content">
        <div class="main__topline">

            <div class="topline__block">
			    <?php
                
					$category_id = get_cat_ID( 'Точка зору' );
					$category_link = get_category_link( $category_id );

                ?>

                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">ТОЧКА ЗОРУ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 4;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsBLOG = get_posts($args);

                    $i = 0;
                    ?>
                    <?php
                    foreach ($postsBLOG as $post) {
                        setup_postdata($post);
                        //if ($post->ID <> $real_id) {
                            if ($i < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                            }
                            $i++;
                        //}
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>


            <div class="topline__block">
				<?php
					$category_id = get_cat_ID( 'Міністерство ветеранів' );
					$category_link = get_category_link( $category_id );	
				?>
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">МІНІСТЕРСТВО ВЕТЕРАНІВ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 6;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsMINVET = get_posts($args);

                    $i2 = 0;
                    ?>
                    <?php
                    foreach ($postsMINVET as $post) {
                        setup_postdata($post);
                    //if ($post->ID <> $real_id) {
                            if ($i2 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i2++;
                            }
                        //    }
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>

            <div class="topline__block">
				<?php
					$category_id = get_cat_ID( 'Фронт' );
					$category_link = get_category_link( $category_id );	
				?>
                <h2 class="topline__header"><a href="<?php echo $category_link; ?>">ФРОНТ</a></h2>
                <div class="topline__carousel-container owl-carousel">
                    <?php
                    $category = get_the_category();
                    rsort($category);

                    $cat_add_id = 5;
                    $real_id = get_the_ID();

                    $args = array('cat' => $cat_add_id);
                    $postsFRONT = get_posts($args);

                    $i3 = 0;
                    ?>
                    <?php
                    foreach ($postsFRONT as $post) {
                        setup_postdata($post);
                    //if ($post->ID <> $real_id) {
                            if ($i3 < 3) {
                                ?>
                                <div class="topline__post-wrapper">
                                    <a href="<?php the_permalink(); ?>" class="post__link btn">Читати більше</a>
                                    <div class="topline__post">
                                        <?php the_post_thumbnail(); ?>
                                        <h3 class="post__heading">
                                            <a href="<?php the_permalink(); ?>" class="post__title">
                                                <?php the_title(); ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                                $i3++;
                            }
                        //  }
                    }
                    ?>
                </div>
                <?php
                wp_reset_postdata();
                ?>
                <div class="carousel__controls">
                    <span class="controls__prev"></span>
                    <a href="<?php echo $category_link; ?>" class="btn controls__all-posts">Усі новини</a>
                    <span class="controls__next"></span>
                </div>
            </div>
        </div>

        <div class="main-wrapper">
            <section class="main__column">
                <div class="topline__header">
                    <?php
                         the_post_thumbnail();
                    ?>
                    <a href="<?php the_permalink(); ?>" style="color: black">
                        <?php the_title(); ?>
                    </a>
                </div>
                <div class="news-feed">
                    <?php while (have_posts()) {
                        the_post(); ?>
                        <div class="news__post">
                            <div class="post-wrapper">
                                
                                <div class="post__content">
                                    <div>
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="post__info">
                                        <div class="tag-list">
                                            <?php

                                            if (get_the_tag_list()) {
                                                echo get_the_tag_list('<ul class="tag_list"><li><span>', '</span></li><li><span>', '</span></li></ul>');
                                            }

                                            ?>

                                        </div>
                                    </div>
                                    <p class="post__date"><?php the_time('F jS, Y'); ?></p>
                                    <div class="post__controls">
                                        <div class="share-list">
                                            <?php
                                            if (function_exists('dynamic_sidebar'))
                                                dynamic_sidebar('space-for-important');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="pagination">
                    <?php the_posts_pagination(); ?>
                </div>
            </section>
<!--            <section class="side__column">-->
<!--                <div class="side__block side__block--video">-->
<!--                    <h2 class="topline__header">Відео</h2>-->
<!--                    --><?php
//                    if (function_exists('dynamic_sidebar'))
//                        dynamic_sidebar('space-for-video');
//                    ?>
<!--                    <a href="/відео/" class="btn btn--all-video">Більше відео</a>-->
<!--                </div>-->
<!---->
<!--                <div class="side__block side__block--news-feed">-->
<!--                    <h2 class="topline__header">Важливе</h2>-->
<!---->
<!--                    --><?php
//                    $category = get_the_category();
//                    rsort($category);
//
//                    $cat_add_id = 7;
//                    $real_id = get_the_ID();
//
//                    $args = array('cat' => $cat_add_id);
//                    $posts = get_posts($args);
//
//                    $i = 0;
//
//                    foreach ($posts as $post) {
//                        setup_postdata($post);
//                        if ($post->ID <> $real_id) {
//                            if ($i < 3) {
//                                ?>
<!--                                <div class="news__post">-->
<!--                                    <div class="post-wrapper">-->
<!--                                        --><?php
//                                        the_post_thumbnail();
//                                        ?>
<!--                                    </div>-->
<!--                                    <a href="--><?php //the_permalink(); ?><!--" class="post__title">-->
<!--                                        <h1>-->
<!--                                            --><?php //the_title(); ?>
<!--                                        </h1>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                                --><?php
//                                $i++;
//                            }
//                        }
//                    }
//                    wp_reset_postdata();
//                    ?>
<!---->
<!--                </div>-->
<!--            </section>-->
            <?php get_sidebar(); ?>

        </div>
    </div>

</main>
<?php
get_footer();
?>

</body>
</html>