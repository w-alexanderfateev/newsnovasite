<section class="side__column">
	<div class="side__block side__block--video">
        <!--<h2 class="topline__header"><a href="/konkurs/">Всеукраїнський Конкурс дитячих віршів та малюнків "Мрії про Україну"</a></h2>-->
        <img src="<?php echo bloginfo('template_url'); ?>/assets/img/nova-img/konkurs-img.jpg" alt="eventPreview-img" class="eventPreview-img">
        <a href="/konkurs/" class="btn btn--all-video">Детальнiше</a>
    </div>
		
    <div class="side__block side__block--subscription">
        <div>
			<h2 class="topline__header">Блог</h2>
			<?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
        </div>
    </div>
	
    <div class="side__block side__block--video">
        <h2 class="topline__header"><a href="/video/">Відео</a></h2>
        <?php
            echo do_shortcode('[sp_html5video category=”3” limit=”1″]');
        ?>
		<?php
            echo do_shortcode('[sp_html5video limit=”1″]');
        ?>
        <a href="/video/" class="btn btn--all-video">Більше відео</a>
    </div>
<!-- 	<div class="side__block side__block--video">
        <h2 class="topline__header"><a href="/video/">Відео</a></h2>
        <?php
            //echo do_shortcode('[sp_html5video category=”3” limit=”1″]');
        ?>
        <a href="/video/" class="btn btn--all-video">Більше відео</a>
    </div> -->

    <div class="side__block side__block--subscription">
        <div>
            <?php echo do_shortcode ("[USM_form]");?>
        </div>
    </div>
    <div class="side__block side__block--news-feed">
        <h2 class="topline__header">Важливе</h2>

        <?php
        $category = get_the_category();
        rsort($category);

        $cat_add_id = 7;
        $real_id = get_the_ID();

        $args = array('cat' => $cat_add_id);
        $posts = get_posts($args);

        $i = 0;

        foreach ($posts as $post) {
            setup_postdata($post);
            if ($post->ID <> $real_id) {
                if ($i < 3) {
                    ?>
                    <div class="news__post">
                        <div class="post-wrapper">
                            <?php
                            the_post_thumbnail();
                            ?>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="post__title">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                        </a>
                    </div>
                    <?php
                    $i++;
                }
            }
        }
        wp_reset_postdata();
        ?>

    </div>
</section>