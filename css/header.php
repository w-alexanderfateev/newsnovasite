<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>НОВА - новини ветеранів: інформаційний портал для учасників АТО/ООС</title>
    <?php
    register_sidebars();
    wp_head();
    ?>

</head>
<body>
<header class="page-header">
    <div class="content">
        <a href="/" class="header__logo">
            <img src="<?php echo bloginfo('template_url'); ?>/assets/img/svg-icons/logo.png" alt="NovaLogo"
                 class="logo__img">
        </a>
        
        <nav class="header__nav header__nav--desktop">
            <ul class="nav__list">
                <?php
                $menu_name = 'top';
                $locations = get_nav_menu_locations();

                if ($locations && isset($locations[$menu_name])) {
                    $menu = wp_get_nav_menu_object($locations[$menu_name]);

                    $menu_items = wp_get_nav_menu_items($menu);

                    foreach ((array)$menu_items as $key => $menu_item) {
                        ?>
                        <li class="nav__item"><a href="<?php echo $menu_item->url; ?>"
                                                 class="nav__link"> <?php echo $menu_item->title; ?></a></li>
                        <?php
                    }
                } else {
                    echo "menu not found, you have to write developers";
                }
                ?>
            </ul>
        </nav>
        <form method="get" action="#" class="header__search">
                <?php
                if (function_exists('dynamic_sidebar'))
                    dynamic_sidebar('space-for-search');
                ?>
        </form>
        <span class="nav__open"><span></span></span>
        <nav class="header__nav header__nav--mobile">

           

            <div class="nav__wrapper">
                <nav>
                    <ul class="nav__list">
                        <?php
                        $menu_name = 'top';
                        $locations = get_nav_menu_locations();

                        if ($locations && isset($locations[$menu_name])) {
                            $menu = wp_get_nav_menu_object($locations[$menu_name]);

                            $menu_items = wp_get_nav_menu_items($menu);

                            foreach ((array)$menu_items as $key => $menu_item) {
                                ?>
                                <li class="nav__item"><a href="<?php echo $menu_item->url; ?>"
                                                         class="nav__link"> <?php echo $menu_item->title; ?></a></li>
                                <?php
                            }
                        } else {
                            echo "menu not found, you have to write developers";
                        }
                        ?>
                    </ul>
				</nav>
            </div>
        </nav>
    </div>
</header>